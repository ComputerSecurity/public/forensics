# Documentation

## Forensics cheatsheet

Contains an up-to-date forensics cheatsheet, the source of the cheatsheet can be found on https://gitlab.cern.ch/ComputerSecurity/public/forensics-cheatsheet

This cheat sheet has been built with our internal Cheat Sheet builder tool: https://gitlab.cern.ch/ComputerSecurity/public/tools/cheatsheet-builder
