# Forensics toolkit

This contains the full linux forensics package as produced by our project at <https://gitlab.cern.ch/ComputerSecurity/public/forensics-toolkit>

If you require just some of the tools, please visit the toolkit project.

These typically are a set of statically compiled binaries so that it can be used in data collection / early investigation stages.


